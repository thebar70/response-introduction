<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('persons')->insert([
            'name' => 'Juan Perez',
            'telephone' => '3012202696',
        ]);
        DB::table('vehicles')->insert([
            'person_id'=>1,
            'model' => '2020',
            'license_plate' => 'PNX-25D',
        ]);
        DB::table('vehicles')->insert([
            'person_id'=>1,
            'model' => '79',
            'license_plate' => 'UNZ-29',
        ]);
    }
}
