<?php

namespace App\ActionClass;

use App\Models\Person;
use App\Result\Api\Result;

class ShowPersonAction
{
    public static function execute($id)
    {
        $result = new Result();
        $person = Person::find($id);
        if ($person) {

            $result->addData('person', $person);
            $result->addMessage('[FOUND] # person was found');
            $result->setStatus('SUCCESS');
            $result->setCode(200);
        } else {
            $result->addMessage('[NOT_FOUND] # person not found');
            $result->setStatus('FAILED');
            $result->setCode(404);
        }

        return $result;
    }
}
