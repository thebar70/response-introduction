<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicles extends Model
{
    protected $table = 'vehicles';
    protected $fillable = ['model', 'color','license_plate'];

    public function person()
    {
        return $this->belongsTo('App\Models\Domicilie');
    }
}
