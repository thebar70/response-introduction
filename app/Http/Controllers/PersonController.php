<?php

namespace App\Http\Controllers;

use App\ActionClass\ShowPersonAction;
use App\Http\Resources\PersonResource;
use App\Traits\Api\ApiResponserTrait;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    use ApiResponserTrait;

    public function showPerson($id)
    {
        $response = null;
        $result = ShowPersonAction::execute($id);

        switch ($result->getStatus()) {
            case 'SUCCESS':
                $response = $this->successResponse(
                    new PersonResource($result->getData('person')),
                    $result->getAllMessage(),
                    $result->getCode(),
                    'resource found',
                );
                break;
            case 'FAILED':
                $response = $this->errorResponse(
                    $result->getAllError(),
                    $result->getAllMessage(),
                    $result->getCode(),
                    'resource not found',
                );
                break;
            default:
                # code...
                break;
        }

        return $response;
    }
}
